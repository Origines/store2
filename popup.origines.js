function insertion_teintes() {
	var teintes_contener = document.getElementById("choice_opt_1");
	var tab_teintes = document.createElement("div");
	var teinte_label;
	if (document.getElementById("contener_tab")) {
		document.getElementById("contener_tab").appendChild(tab_teintes);
		if (document.getElementById("video")) {
			tab_teintes.outerHTML = "<div onclick=\"SFselectTab('briefcase_product', '5');document.getElementById('video').pause();\" id=\"tab5\" data-name=\"titreTab\" class=\"tab\">" + Vteintes + "</div>";
		} else {
			tab_teintes.outerHTML = "<div onclick=\"SFselectTab('briefcase_product', '5');\" id=\"tab5\" data-name=\"titreTab\" class=\"tab\">" + Vteintes + "</div>";
		}
		var content_teintes = document.createElement("div");
		document.getElementById("contener_content").style.height = "330px";
		document.getElementById("contener_content").appendChild(content_teintes);
		content_teintes.outerHTML = "<div id=\"contentTab5\" data-name=\"contentTab\" class=\"content\"><div id= \"teintes\"></div></div></div>";
		document.getElementById("contentTab5").insertBefore(teintes_contener, document.getElementById(teintes));
		SFselectTab('briefcase_product', '5');
		var spans2 = document.querySelectorAll(".hover");       
		for (i = 0; i < spans2.length; i++) {
			if (spans2[i].parentNode.className == "opt_instock") {
				var chaine = spans2[i].getAttribute("onclick");
				spans2[i].setAttribute("onclick", chaine + ";teinte_on(this)");
			} else {
				spans2[i].removeAttribute("onclick");
			}
		}
		teintes_contener.style.visibility = "visible";
		if (document.getElementById("teintes")) {
			document.getElementById("briefcase_product").setAttribute("class", "briefcase_product_teintes");
		}
		if (document.getElementById("input_choice_opt_1")) {
			teinte_label = document.querySelectorAll("span.opt_label");
			for (i = 0; i < teinte_label.length; i++) {
				if (document.getElementById("input_choice_opt_1").value == teinte_label[i].innerHTML) {
					teinte_label[i].parentNode.className = "teintes_on";
				}
			}
			teinteTabContenance();
		}
		teinte_label = document.querySelectorAll("span.opt_label");
		if (teinte_label) {
			for (var i = 0; i < teinte_label.length; i++) {
				if (/^###/.test(teinte_label[i].innerHTML)) {
					var parent = teinte_label[i].parentNode;
					parent.parentNode.style.display = "none";
				}
			}
		}
		var newteintes = document.querySelectorAll(".bskrequired");
		var isTeintes = document.querySelectorAll("li.opt_instock");
		if (!isTeintes[0]) {
			document.getElementById("briefcase_product").className = document.getElementById("briefcase_product").className + " briefcase_no_teintes";
			document.getElementById("choice_opt_1").className = document.getElementById("choice_opt_1").className + " no-teinte";
			document.getElementById("product").className = document.getElementById("product").className + " product-no-teinte";
		} else {
            if(!isTeintes[1]){
                for (i = 0; i < newteintes.length; i++) {
                    newteintes[i].style.display = "none";
                }
            }
		}
		if (paramUrl("#teinte") == true) {
			parameters = getUrlVars(url.substring(url.indexOf("?") + 1), "#teinte=&on");
			if (parameters[1].indexOf("%") > -1) {
				parameters[1] = parameters[1].replace("%", "'");
			}
            if (parameters[1].indexOf("*") > -1) {
                parameters[1] = parameters[1].replace("*", "°");
			}
			console.log(parameters[1]);
			var teinte_on = parameters[1];
			teinte_on = teinte_on.split("+").join(" ");
			var opt_label = document.querySelectorAll("span.opt_label");
			var P_opt_label;
			if (opt_label) {
				for (i = 0; i < opt_label.length; i++) {
					P_opt_label = opt_label[i].parentNode;
					if (opt_label[i].innerHTML.toLowerCase().replace(new RegExp(/[èéêë]/g), "e") == teinte_on.toLowerCase()) {
						if (P_opt_label.parentNode.className != "zerostock bskrequired") {
							P_opt_label.onclick();
						} else {
							document.getElementById("alertStock").style.display = "block";
						}
					}
				}
			}
		}
	} else {
		if (document.getElementById("contenances_contener")) {
			document.getElementById("detail").style.minHeight = "0px";
			document.getElementById("contenances_contener").style.marginTop = "200px";
			if (document.getElementById("list")) {
				document.getElementById("list").style.marginTop = "115px";
			}
            if(document.getElementById("option_contener")){
                document.getElementById("option_contener").style.cssFloat = "right";
                document.getElementById("option_contener").style.marginRight = "80px";
            }		
            if(document.getElementById("detail") && document.getElementById("detail").getElementsByTagName("p")[0]){
                document.getElementById("detail").getElementsByTagName("p")[0].style.textAlign = "right";
                document.getElementById("detail").getElementsByTagName("table")[0].style.cssFloat = "right";
            }			
			var option_pic = document.getElementById("choice_opt_1").querySelectorAll("img.option_pic");
			if (option_pic) {
				for (i = 0; i < option_pic.length; i++) {
					option_pic[i].style.height = "70px";
					option_pic[i].style.width = "70px";
				}
			}
			var opt_instock = document.querySelectorAll("li.opt_instock");
			if (opt_instock) {
				for (i = 0; i < opt_instock.length; i++) {
					opt_instock[i].style.width = "240px";
				}
			}
		}
	}
	videoProduct();
}

function contenerChangeMob(id) {
    var primary = id.parentNode;
    var href = primary.parentNode.getElementsByTagName("span")[1].innerHTML;
    var price = primary.parentNode.getAttribute("data-pdtprice");
    var pdtid = primary.parentNode.getAttribute("data-pdtid");
    if (document.getElementById("parkod_contenances")) {
        price = price + " €";
    }
    var stock = primary.parentNode.getElementsByTagName("span")[13].innerHTML;
    document.getElementById("contenance_mob").innerHTML = id.innerHTML;
    document.getElementById("secondary").innerHTML = href;
    document.getElementById("change_img_hd").value = pdtid;
    document.getElementById("image").className = "view view_" + pdtid;    
    if(isMarque("dior") == true){
        var img0 = new Image();
        idImg = id.parentNode.parentNode.getAttribute("data-pdtcode");       
        img0.src = "https://www.oui-lemag.com/Images/Produits/" + idImg + "2.jpg";        
        document.getElementById("img1").src = img0.src;   
    }else{        
        if (document.getElementById("img1").src.indexOf("jpg") != -1) {
            document.getElementById("image_zoom").getElementsByTagName("img")[0].src = "https://www.origines-parfums.com/content/product_" + pdtid + "hd.jpg";
            document.getElementById("img1").src = "https://www.origines-parfums.com/content/product_" + pdtid + "hd.jpg";
        }else{
            document.getElementById("image_zoom").getElementsByTagName("img")[0].src = "https://www.origines-parfums.com/content/product_" + pdtid + "hd.png";
            document.getElementById("img1").src = "https://www.origines-parfums.com/content/product_" + pdtid + "hd.png";
        }
        if (imageExists(document.getElementById("image_zoom").getElementsByTagName("img")[0].src) == false) {
            document.getElementById("image_zoom").getElementsByTagName("img")[0].src = "https://www.origines-parfums.com/img/photo_en_cours-b.jpg";
        } else {
            document.getElementById("image_zoom").getElementsByTagName("img")[0].style.padding = "0px";
        }
        if (imageExists(document.getElementById("img1").src) == false) {
            document.getElementById("image_zoom").getElementsByTagName("img")[0].src = "https://www.origines-parfums.com/img/photo_en_cours-b.jpg";
        }
    }    
    if (document.getElementById("link_primary_on")) {
        document.getElementById("link_primary_on").id = "link_primary_off";
        id.id = "link_primary_on";
    } else {
        id.setAttribute("id", "link_primary_on");
    }
    minContenanceVerifMob();
    document.getElementById("ajaxPriceTTC").innerHTML = price;
    document.getElementById("list_btn").innerHTML = "<span id=stock_pos class=''></span><div id='block_quantity' class='block_quantity'></div>";
    var stockPos =  document.getElementById("stock_pos");    
    stockPos.innerHTML = stock;
    if (stockPos.innerHTML == txtProd1) {
        stockPos.className = "out_stock";
    }
    if (stockPos.innerHTML == "En stock") {
        stockPos.className = "stock_pos";
        primary.parentNode.getElementsByTagName("input")[3].value = txtProd;
        var achat = primary.parentNode.getElementsByTagName("input")[3].outerHTML;
        var block_quantity = primary.parentNode.getElementsByTagName("span")[14].innerHTML;
        primary.parentNode.getElementsByTagName("input")[1].id = "toto";
        document.getElementById("block_quantity").innerHTML = block_quantity;
        document.getElementById("list_btn").innerHTML = document.getElementById("block_quantity").outerHTML + achat;
        document.getElementById("block_quantity").getElementsByTagName("input")[1].id = document.getElementById("block_quantity").getElementsByTagName("input")[1].getAttribute("name");
    }
    if (document.querySelector("div.marque_navig_chanel")) {
        if (document.getElementById("secondary").innerHTML.indexOf("recharge") != -1) {
            document.getElementById("secondary").getElementsByTagName("a")[0].style.visibility = "visible";
            document.getElementById("secondary").getElementsByTagName("a")[0].style.display = "block";
            document.getElementById("secondary").getElementsByTagName("a")[0].style.marginLeft = "8px";
        }
        if (document.getElementById("ajaxPriceTTC").innerHTML.indexOf("€") > -1) {    
            document.getElementById("ajaxPriceTTC").innerHTML =  document.getElementById("ajaxPriceTTC").innerHTML.replace("€","");
        }
    }else{
        if (document.getElementById("ajaxPriceTTC").innerHTML.indexOf("€") == -1) {    
            document.getElementById("ajaxPriceTTC").innerHTML += " €";
        }
    } 
   
    if (paramUrl("#news") == true || window.remProd) {
        if (primary.parentNode.className.indexOf("ok") > -1) {
            prix2("product");
            if (document.getElementById("priceIndicativeActif")) {                
                if (!document.getElementById("contenances_contener_mob").querySelectorAll("span.IT")[0]) {
                    document.getElementById("contenances_contener_mob").querySelectorAll("span.price_pos")[0].innerHTML = document.getElementById("contenances_contener_mob").querySelectorAll("span.price_pos")[0].innerHTML + document.getElementById("priceIndicativeActif").getElementsByTagName("span")[3].outerHTML;
                }
            }
        } else {
            if (document.getElementById("priceIndicativeActif")) {
                document.getElementById("priceIndicativeActif").getElementsByTagName("span")[3].removeAttribute("style");
                document.getElementById("priceIndicativeActif").removeAttribute("id");
                if (document.getElementById("image").querySelectorAll("p.comment")) {
					document.getElementById("image").querySelectorAll("p.comment")[0].innerHTML = " ";
					document.getElementById("image").querySelectorAll("p.comment")[0].removeAttribute("style");
				}                
                document.getElementById("contenances_contener_mob").querySelectorAll("span.price_pos")[0].innerHTML = document.getElementById("ajaxPriceTTC").outerHTML;                
            }
        }
    }
}

function ffCallBackFunc(){    
    if (window.prixRond) {
        if (!$("#ff_crit_att2917").find(".ff_crit_att_checked").length) {
                prix2("prixRond");
                $("#ff_crit_list_att2917").find("li").removeClass("ff_crit_att_checked_active");
        } else {
                $("#ff_crit_att2917").find("li").toggleClass("ff_crit_att_checked_active");
                $(".ff_crit_att_checked").parent().removeClass("ff_crit_att_checked_active");
        }
    } else {
        if (window.prix) {
            prix2();
        }
        if (window.idProd) {
            prix2("oneProdList");
        }
    }
    if(document.getElementById("ff_crit_att3464")){     
        var brands = document.getElementById("ff_crit_att3464");
        var brandsP = document.getElementById("ff_center").getElementsByTagName("div")[2];
        document.getElementById("ff_center").insertBefore(brands,brandsP);
    }
    if (window.compteur) {
        Rebour();
    }
    input_detail();
    if (window.imgRedirection && window.urlRedirection && window.imgRedirectionMob) {
        redirectionBasDePage(imgRedirection, urlRedirection, imgRedirectionMob);
    }
    if (document.getElementById("showcase_lib") || document.getElementById("showcase_lib1")) {
        if (document.getElementById("logo_container")) {
            document.getElementById("logo_container").style.marginBottom = "0px";
        }
    } else {
        if (document.getElementById("banniere_container") && !document.getElementById("banniere_mob") && document.getElementById("mobile").className == "yes_mobile") {
            document.getElementById("banniere_container").style.display = "none";
        }
    }
    if (!document.getElementById("menu_marque") && document.getElementById("banniere_mob") && document.getElementById("logo_container")) {
        document.getElementById("logo_container").setAttribute("class", "logo_container_marques");
    }
    jquery_load();
}

function popupVideo(elm,btn){	
    btn.addEventListener("click", function(e){						
	document.getElementById("popup_div_bkjs").getElementsByTagName("div")[2].innerHTML=elm;
	document.getElementById("popup_div_bkjs").style.display="inline";
	document.getElementById("popup_div_bkjs").getElementsByTagName("div")[0].id="popup_video";
	mise_en_page_video_fin();
	e.stopPropagation();
	},false);	
}

function PopupMarketing(name, url, elm, pclass, ouvrantEtat, ouvrantContenu) {
	this.name = name;
	this.url = url;
	this.elm = elm;
	this.pclass = pclass;
	this.ouvrantEtat = ouvrantEtat;
	this.ouvrantContenu = ouvrantContenu;
	var imgPopupCroix = new Image();
	var imgPopupLeft = new Image();
	imgPopupLeft.src = "https://www.origines-parfums.com/media/popup_" + elm + ".png";
	var imgPopupText = new Image();
	imgPopupText.src = "https://www.origines-parfums.com/media/texte_popup_" + elm + ".png";
	var imgPopupMob = new Image();
	var popup;
	if (ouvrantEtat == 3) {
		imgPopupMob.src = "https://www.origines-parfums.com/media/popup_" + elm + "_mob.jpg";
		var imgPopupLogo = new Image();
		imgPopupLogo.src = "https://www.origines-parfums.com/media/popup_logo_" + elm + ".png";
		popup = document.createElement("div");
		popup.setAttribute("id", name);
		if (name == "cookie2") {
			popup.setAttribute("class", "cookie_on");
		} else {
			popup.setAttribute("class", "cookie_off");
		}
		popup.innerHTML = "<div class='popup popup_prestige' id='banniere_container2'><div id='banniere'> </div></div><div class='" + pclass + "'><a href=" + url + "><img alt='' src='" + imgPopupLeft.src + "' /></a></div><div class='popup_texte'><div style='background: url(" + imgPopupLogo.src + ") no-repeat 0% 0%;'><a href=" + url + "><img alt='' src='" + imgPopupText.src + "' /></a></div></div><div class='popup_link'><div><a href=" + url + ">...</a></div>";
		document.getElementById("footer").appendChild(popup);
		initDiv(name, "<p class=texte_avantage><a href=" + url + "><img src=" + imgPopupMob.src + " /></a></p>");
		document.getElementById(name).getElementsByTagName("a")[0].setAttribute("onclick", "ga('send', 'event', 'Clic', 'Clics', 'Popup Prestige " + elm + "')");
		document.getElementById(name).getElementsByTagName("a")[1].setAttribute("onclick", "ga('send', 'event', 'Clic', 'Clics', 'Popup Prestige " + elm + "')");
		document.getElementById(name).getElementsByTagName("a")[2].setAttribute("onclick", "ga('send', 'event', 'Clic', 'Clics', 'Popup Prestige " + elm + "')");
		if (readCookie("originesparfums_" + name) == name) {
			eraseCookie("originesparfums_" + name);
		}
		if (detectBrowser() > 639) {
			document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("table")[0].setAttribute("data-echo", name);
		}
	} else {
		if (pclass.indexOf("gif") > -1) {
			imgPopupMob.src = "https://www.origines-parfums.com/media/popup_" + elm + "_mob.gif";
			imgPopupCroix.src = "https://www.origines-parfums.com/media/sf_pop_close2.jpg";
		} else if (pclass.indexOf("none") > -1) {
			imgPopupMob.src = "https://www.origines-parfums.com/img/pix.gif";
			imgPopupCroix.src = "https://www.origines-parfums.com/media/la_fourmi_croix.png";
		} else {
			imgPopupMob.src = "https://www.origines-parfums.com/media/popup_" + elm + "_mob.jpg";
			imgPopupCroix.src = "https://www.origines-parfums.com/media/la_fourmi_croix.png";
		}
		if (ouvrantEtat < 2) {
			popup = document.createElement("div");
			popup.setAttribute("id", name);
			if (name == "cookie2") {
				popup.setAttribute("class", "cookie_on");
			} else {
				popup.setAttribute("class", "cookie_off");
			}
			popup.innerHTML = "<div class='popup' id='banniere_container2'><div id='banniere'> </div></div><div class='" + pclass + "'><a href=" + url + "><img alt='' src='" + imgPopupLeft.src + "' /></a></div><div class='popup_close'><div><img alt='' src='" + imgPopupCroix.src + "' /></div></div><div class='popup_texte'><div><a href=" + url + "><img alt='' src='" + imgPopupText.src + "' /></a></div></div><div class='popup_close showDiv'><div><a onclick=showDiv('" + name + "')>...</a></div></div><div class='popup_link'><div><a href=" + url + ">...</a></div></div>";
			document.getElementById("footer").appendChild(popup);
			if (pclass.indexOf("decouv") > -1) {
				document.getElementById("banniere_container2").setAttribute("class", "popup popup_decouv");
			}
			if (ouvrantContenu.indexOf("null") == -1) {
				initDiv(name, ouvrantContenu);
				document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("div")[0].setAttribute("id", "sf_pop_ouvrant");
				document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("div")[0].className = "sf_pop sf_pop_height sf_pop_ouvrant";
			} else {
				initDiv(name, "<p class=texte_avantage><a href=" + url + "><img src=" + imgPopupMob.src + " /></a></p>");
			}
			if (readCookie("originesparfums_" + name) == name) {
				eraseCookie("originesparfums_" + name);
			}
			if (ouvrantEtat == 1) {
				document.getElementById(name).getElementsByTagName("img")[0].setAttribute('onclick', 'PopupOuvrant("' + name + '","' + ouvrantContenu + '")');
				document.getElementById(name).getElementsByTagName("img")[2].setAttribute('onclick', 'PopupOuvrant("' + name + '","' + ouvrantContenu + '")');
				document.getElementById(name).getElementsByTagName("a")[3].setAttribute('onclick', 'PopupOuvrant("' + name + '","' + ouvrantContenu + '")');
				document.getElementById(name).addEventListener("click", function () {
					if (document.getElementById("alertMiniQuantityPurchase")) {
						document.getElementById("alertMiniQuantityPurchase").className = "pop_up pop_alert";
						if (document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("table")[0].hasAttribute("data-echo") == true) {
							if (document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("table")[0].hasAttribute("data-id") == true) {
								document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("table")[0].removeAttribute("data-id");
								document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("table")[0].removeAttribute("id");
								document.getElementById("sf_pop_ouvrant").className = "sf_pop sf_pop_height";
							}
						}
					}
				});
				if (document.getElementById("banniere_container") && detectBrowser() > 639) {
					if (document.getElementById("banniere") && !document.getElementById("banniere").getElementsByTagName("a")[0]) {
						document.getElementById("banniere_container").style.cursor = "pointer";
						document.getElementById("banniere_container").addEventListener("click", function () {
							PopupOuvrant(name, ouvrantContenu);
						});
					}
				}
			}
			if (detectBrowser() > 639) {
				document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("table")[0].setAttribute("data-echo", name);
			}
		}
		if (ouvrantEtat == 2) {
			/*document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("div")[0].setAttribute("id", "sf_pop_ouvrant");
			document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("div")[0].className = "sf_pop sf_pop_height sf_pop_ouvrant";*/          
            document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("div")[0].className = "sf_pop sf_pop_height";
			PopupOuvrant(name, ouvrantContenu);
            document.getElementById("alertMiniQuantityPurchase").getElementsByTagName("div")[0].removeAttribute("id");
		}
	}
}

function ongletSupp(title, text, type){
    this.title = title;
	this.text = text;
    this.type = type; 
    /*type : 0 -> onglet oca on / type : 1 -> onglet oca off /  
      type : 2 -> onglet video on / type : 3 -> onglet video off*/    
    
	if (document.getElementById("product") && detectBrowser() > 639) {
        var tab0, tab1;
		var nTab = 3;
		var nTabEtat = !document.getElementById("tab3") ? nTab = 3 : (!document.getElementById("tab4") ? nTab = 4 : nTab = 5);
		var tabOnglet = document.createElement("div");
		tabOnglet.setAttribute("id", "tab" + nTab);
		tabOnglet.setAttribute("class", "tab");
		tabOnglet.setAttribute("data-name", "titreTab");        
        
        if(type==2 || type==3){
            if(document.getElementById("feature") && document.getElementById("feature").querySelector(".pd01")){                
                tabO = document.getElementById("tab1").getAttribute("onclick").replace("SFselectTab('briefcase_product', '1');", "");
                
                tabOnglet.setAttribute("onclick", "SFselectTab('briefcase_product', '" + nTab + "');tabO;document.getElementById('contener_content').style.height='100%';document.getElementById('list').className='associed videoOn';isZoomWithVideo(1);document.getElementById('feature').className='videoOn';");
                var i =1;
                if(!document.getElementById("tab2")){
                    j =  Number(document.getElementById("tab1").id.replace("tab",""));
                }else{
                    j =  Number(document.getElementById("contener_tab").lastChild.previousSibling.id.replace("tab",""));
                }              
                var tab = document.getElementById("contener_tab").querySelectorAll("#tab"+i);                   
                for(i=1;i<j+1;i++){
                    if(i != nTab){
                        tab1 = document.getElementById("tab"+i).getAttribute("onclick");
                        document.getElementById("tab"+i).setAttribute("onclick",tab1+";document.getElementById('contener_content').removeAttribute('style');document.getElementById('list').className='associed';document.getElementById('feature').className='';document.getElementById('contener_content').style.overflow='hidden';document.getElementById('video').pause();isZoomWithVideo(0);");
                    }
                }
            }else{            
                tabO = document.getElementById("tab1").getAttribute("onclick").replace("SFselectTab('briefcase_product', '1');", "");
                tabOnglet.setAttribute("onclick", "SFselectTab('briefcase_product', '" + nTab + "');tabO;document.getElementById('contener_content').style.height='100%';document.getElementById('list').className='associed videoOn';isZoomWithVideo(1);");
            }
            
        }else{        
            if (document.getElementById("video")) {
                tabO = document.getElementById("tab1").getAttribute("onclick").replace("SFselectTab('briefcase_product', '1');", "");
                tabOnglet.setAttribute("onclick", "SFselectTab('briefcase_product', '" + nTab + "');"+tabO+" ");  
                videoProduct();
            } else {
                tabOnglet.setAttribute("onclick", "SFselectTab('briefcase_product', '" + nTab + "')");
            }
        }
		tabOnglet.innerHTML = title;
		if (document.getElementById("tab5")) {
			document.getElementById("contener_tab").insertBefore(tabOnglet, document.getElementById("tab5"));
		} else {
			document.getElementById("contener_tab").appendChild(tabOnglet);
		}
        
		var tabContentOnglet = document.createElement("div");
		tabContentOnglet.setAttribute("id", "contentTab" + nTab);
		tabContentOnglet.setAttribute("class", "content");
		tabContentOnglet.setAttribute("data-name", "contentTab");
		tabContentOnglet.innerHTML = text;
		document.getElementById("contener_content").appendChild(tabContentOnglet);
		if (type == 0) {        
			SFselectTab('briefcase_product', nTab);
		}
        if (type == 2 || type == 3) {    
            document.getElementById("contentTab"+nTab).style.textAlign = "center";    
        }
        if (type == 2) {             
			document.getElementById("tab"+nTab).click();
		}
	}
}

function setBasket(pdtId, action, param){
	var xhr_object = null;
	xhr_object = getHTTPObject();
	xhr_object.open("GET", "/mag/setBasket.php?id="+pdtId+"&type="+action+"&rnd="+Math.random()+param, true); 
	xhr_object.onreadystatechange = function() { 
		if(xhr_object.readyState == 4) {
            var reponse = xhr_object.responseText;

            if (document.getElementById('item_basket')) {
                getBasket(getValueFromParams(param, "lang"));
            }  

            if (action != "del" && action != "rem") {
                if (getValueFromParams(param, "bid") != null) {
                    if (document.getElementById('id_btn_getprestation'))
                        document.getElementById('id_btn_getprestation').setAttribute('disabled', 'disabled');
                    document.getElementById('popup_div_reservation').style.display = 'inline';
                } else {
                    var instock = getValueFromParams(param, "instock");
                    var stockorder = getValueFromParams(param, "nb");
                    instock    = parseInt(instock);
                    stockorder = parseInt(stockorder);

                    if (getValueFromParams(param, "bid") != null && stockorder > instock) {
                        popup_alert('stocklimit', 'popup_div_stocklimit', instock)
                        setTimeout("document.getElementById('popup_div_stocklimit').style.display = 'none';", 2250);
                    } else {
                        var popup_inactive = getValueFromParams(param, "popup_inactive");

                        if (!popup_inactive) {
                            if (stockorder > instock) {
                                document.getElementById('popupbkjs_stockinsufficient').style.display = 'block';
                                document.getElementById('popupbkjs_stockinsufficient_number').innerHTML = instock;

                                document.getElementById('popupbkjs_quantity').innerHTML = 'x '+instock;
                            } else {
                                document.getElementById('popupbkjs_stockinsufficient').style.display = 'none';

                                document.getElementById('popupbkjs_quantity').innerHTML = 'x '+stockorder;
                            }

                            var basketOnglet = '';

                            if (document.getElementById('product')) {
                                if (document.getElementById('briefcase_popup_basket')) {
                                    basketOnglet = document.getElementById('briefcase_popup_basket').innerHTML;
                                } else if (document.getElementById('list_contener') && document.getElementById('pdt_popup_basket')) {
                                    basketOnglet = document.getElementById('title_associed').outerHTML;
                                    for (var i = 1; i <= 3; i++) {
                                        if (document.getElementById('item' + i))
                                            basketOnglet += document.getElementById('item' + i).outerHTML;
                                    }
                                }
                                basketOnglet = '<div class="content_popup_bsk_onglet">' + basketOnglet + '</div>';
                                sf_innerHTML('content_popup_bsk_onglet', basketOnglet);
                            }                           
                            
                            if(document.getElementById("product") && document.getElementById("feature") && document.getElementById("feature").querySelector("p.pd01")){  
                                document.getElementById("feature").querySelector("input.btn_buy").addEventListener("click",function(){
                                    console.log("ok");
                                });
                                var cond1 = 'Votre souvenir ici (22 caractères max espaces compris)';
                                var cond2 = "Votre nom ici ou celui d'un être cher (22 caractères max espaces compris)"; 
                                
                                if(((document.getElementById('ctx_perso0').value == cond1) == false) && ((document.getElementById('ctx_perso1').value == cond2) == false)) {
                                    reponse = reponse.replace("</span></a></div>",' "'+document.getElementById('ctx_perso0').value+' | '+document.getElementById('ctx_perso1').value+'"');                                 
                                    document.getElementById('ctx_perso').value=document.getElementById('ctx_perso0').value+' | '+document.getElementById('ctx_perso1').value;                                     
                                }                                                                                         
                            }
                            
                            sf_innerHTML('content_popup_div_bkjs', reponse);
                      
                            document.getElementById('popup_div_bkjs').style.display = 'inline';
                        }

                    }
				}
			}
		}
	}
	xhr_object.send(null);
}

function popupActivation() {
	var date_limite = new Date();
	var jour_limite = date_limite.getDate();
	var heure_limite = date_limite.getHours();
	var mois_limite = date_limite.getMonth();
	var popup;
	var popupAll = 1;
	var tabNavig = [];
	var tabNavig2 = [];
	if (document.getElementsByTagName("meta")[1] && document.getElementsByTagName("meta")[1].getAttribute("content") == "fr") {	 
        
        /*Début de la liste de Victoria*/ 
        /* onglet visible sur tout l'espace guerlain : https://www.origines-parfums.com/guerlain.htm*/
        if(isMarque("guerlain") == true && detectBrowser()>639){          
            var guerlainTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/Miniature_LPRN_EDT_guerlain_250x250.jpg' style='width: 200px !important; float: left; height: 200px;'></p><p class='texte_cadeau' style='width:465px'>Votre privilège Exclusif GUERLAIN</p><p class='texte_cadeau' style='width:465px'><br>Votre miniature La Petite Robe Noire Eau de Toilette OFFERTE<br>dès 75€ d'achat dans la marque Guerlain !</p><p class='texte_cadeau' style='width:465px'><br>Votre code cadeau :<span style='color:#dc4b5f;'> PETITEROBE</span></p><p class='texte_cadeau' style='width:465px'><br><small>Offre valable jusqu'au 27 mai dans la limite des stocks disponibles. </small></p><p class='texte_cadeau' style='width:465px'><small>A renseigner dans la case prévue à cet effet, lors de la validation de la commande. </small></p><p class='texte_cadeau' style='width:465px'><span style='font-size:11px;'><small><em>Photo non contractuelle.</em></small></span></p>";
            var guerlainOnglet = new ongletSupp("Votre Cadeau Spécial Fête des Mères", guerlainTexte, 0);             
           
            popupAll = 0;
            popup = new PopupMarketing("guerlain", "http://bit.ly/2HSm9Pw", "guerlain", "popup_parfum popup_parfum_auto popup_decouv popup_decouv_transparent", 0, "null"); 
        }    
        if(whereAreYou("list_663598")==true && detectBrowser()>639){             
            popupAll = 0;
            popup = new PopupMarketing("cookie", "http://bit.ly/2HSm9Pw", "guerlain", "popup_parfum popup_parfum_auto popup_decouv popup_decouv_transparent", 0, "null"); 
        }      
        /* bandeau visible sur tout l'espace Dior : https://www.origines-parfums.com/dior.htm*/
		if(isMarque("dior") == true){    
            var diorTexte = "<p class='image_cadeau' style='margin-left: 0px;'><img src='https://www.origines-parfums.com/media/gwp_dior_injoy_240x240.png' style='width: 200px; float: left; height: 200px;' /></p><p> </p><p class='texte_cadeau' style='width: 420px;'><strong style='font-weight:bold !important;'>LA MAISON DIOR </strong></p><p class='texte_cadeau' style='width: 420px;'><strong style='font-weight:bold !important;'>A le plaisir de vous offrir votre miniature</strong></p><p class='texte_cadeau' style='width: 420px;'><strong style='font-weight:bold !important;'>J'ADORE INJOY</strong></p><p class='texte_cadeau' style='width: 420px;'><strong style='font-weight:bold !important;'>dès l'achat d'un parfum dans la marque DIOR.</strong></p><p class='texte_cadeau' style='width: 420px;'><br />Votre code cadeau :<span style='color:#a18152;'> <strong>DIOR</strong></span></p><p class='texte_cadeau' style='width: 420px;'> </p><p class='texte_cadeau' style='width: 420px;'><small>Offre valable dans la limite des stocks disponibles. </small></p><p class='texte_cadeau' style='width: 420px;'><small>Photo non contractuelle. </small></p>";
            var diorOnglet = new ongletSupp("SPÉCIAL FÊTE DES MÈRES", diorTexte, 0);  
            
            /*popupAll = 0;
            popup = new PopupMarketing("dior", "http://bit.ly/2ra3pA5", "dior", "popup_parfum popup_parfum_auto popup_decouv popup_decouv_transparent", 0, "null");*/
        }
		/*Fin de la liste de Victoria*/		
		
		/*Début de la liste des bandeaux, popups et onglets supplémentaires à voir avec Chrsitelle*/
		 /*if(isMarque("kenzo") == true){            
            popupAll = 0;
            popup = new PopupMarketing("kenzo", "javascript:void(0)", "kenzohomme_oca", "popup_parfum popup_parfum_auto popup_decouv popup_decouv_transparent", 0, "null"); 
        }*/ 
        /* onglet visible sur tout l'espace jean-paul gaultier : https://www.origines-parfums.com/jean-paul-gaultier.htm*/
        if(isMarque("jean-paul-gaultier") == true){  
            var jeanTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/GWP_onglet_tote_jpg.png' style='width: 200px !important; float: left; height: 200px;' /></p><p class='texte_cadeau' style='width:430px'><br />Votre tote bag Jean Paul Gaultier OFFERT dès l'achat d'un vaporisateur 50 ml dans la marque.</p><p class='texte_cadeau' style='width:430px'><br />Votre code cadeau :<span style='color:#00afda;'> ILOVEGAULTIER</span></p><p class='texte_cadeau' style='width:430px'><br /><small>* Valable jusqu'au 18 juin dans la limite des stocks disponibles. </small></p><p class='texte_cadeau' style='width:430px'> </p>";
            var jeanOnglet = new ongletSupp("Votre Cadeau", jeanTexte, 0);         
        }
        
        /* bandeau + onglet visible sur tout l'espace acqua di parma : https://www.origines-parfums.com/acqua-di-parma.htm*/
        if(isMarque("acqua-di-parma") == true){  
            var acquaTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/gwp_ponglet_Miniature_pack.png' style='width: 200px !important; float: left; height: 200px;' /></p><p class='texte_cadeau' style='width:430px'><br />Votre miniature Peonia Nobile OFFERTE pour tout achat dans la marque ACQUA DI PARMA.</p><p class='texte_cadeau' style='width:430px'><br />Votre code cadeau :<span style='color:#ca64a0;'> PEONIA</span></p><p class='texte_cadeau' style='width:430px'><br /><small>* Valable jusqu'au 27 mai dans la limite des stocks disponibles. </small></p><p class='texte_cadeau' style='width:430px'> </p>";
            var acquaOnglet = new ongletSupp("Votre Cadeau", acquaTexte, 0);  
            
            popupAll = 0;
            popup = new PopupMarketing("acqua-di-parma", "javascript:void(0)", "acqua_di_parma", "popup_parfum popup_parfum_auto", 0, "null");
        }
        
        /* bandeau sur parfums rares : https://www.origines-parfums.com/parfums-rares.htm*/
        if(whereAreYou("list_670616") == true){    
            popupAll = 0;
            popup = new PopupMarketing("list_670616", "javascript:void(0)", "acqua_di_parma", "popup_parfum popup_parfum_auto", 0, "null");
        }
        
        /* bandeau + onglet visible sur tout l'espace Repetto : https://www.origines-parfums.com/repetto.htm*/
        if(isMarque("repetto") == true){  
            var repettoTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/GWP_repetto_collier.png' style='width: 200px !important; float: left; height: 200px;' /></p><p class='texte_cadeau' style='width:430px'><br />Votre collier Repetto OFFERT pour tout achat dans la marque REPETTO.</p><p class='texte_cadeau' style='width:430px'><br />Votre code cadeau :<span style='color:#0f2b4d;'> REPETTO</span></p><p class='texte_cadeau' style='width:430px'><br /><small>* Valable jusqu'au 25 mai dans la limite des stocks disponibles. </small></p><p class='texte_cadeau' style='width:430px'> </p>";
            var repettoOnglet = new ongletSupp("Votre Cadeau", repettoTexte, 0);  
            
            popupAll = 0;
            popup = new PopupMarketing("repetto", "javascript:void(0)", "repetto", "popup_parfum popup_parfum_auto", 0, "null");
        } 	
		/*Fin de la liste de Christelle*/
		
		/*Début de la liste des bandeaux, popups et onglets supplémentaires à voir avec Laurie*/
        
        /* bandeau + onglet visible sur tout l'espace Armani : https://www.origines-parfums.com/armani.htm*/
        /*if(isMarque("armani") == true){ 
            var armaniTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/onglet_supp_adgh_absolu.png' style='width: 200px !important; float: left; height: 200px;' /></p><p class='texte_cadeau' style='width:475px'><br />Votre miniature Acqua Di Gio Absolu 5 ml OFFERTE dès 65€ d'achats de la marque Giorgio Armani.</p><p class='texte_cadeau' style='width:475px'><br />Votre code cadeau :<span style='color:#a43e26;'> ABSOLU </span></p><p class='texte_cadeau' style='width:475px'><br /><small>A renseigner dans la case prévue à cet effet, lors de la validation de la commande. </small></p><p class='texte_cadeau' style='width:475px'><small>Offre valable jusqu'au 17 juin dans la limite des stocks disponibles. </small></p>";
            var armaniOnglet = new ongletSupp("Mon Cadeau", armaniTexte, 0);         
            
            popupAll = 0;
            popup = new PopupMarketing("armani", "javascript:void(0)", "adgh_absolu", "popup_parfum popup_parfum_auto", 0, "null");
        }*/
        
        /* bandeau + onglet visible sur tout l'espace Yves Saint Laurent : https://www.origines-parfums.com/yves-saint-laurent.htm*/
        /*if(isMarque("ysl") == true){        
            var yslTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/onglet_supp_ysl_cologne.png' style='width: 200px !important; float: left; height: 200px;' /></p><p class='texte_cadeau' style='width:475px'><br />Votre gel douche L'Homme Yves Saint Laurent 50 ml OFFERT pour tout achat d'un parfum Homme dans la marque Yves Saint Laurent.</p><p class='texte_cadeau' style='width:475px'><br />Votre code cadeau :<span style='color:#098bab;'> BLEU </span></p><p class='texte_cadeau' style='width:475px'><br /><small>A renseigner dans la case prévue à cet effet, lors de la validation de la commande. </small></p><p class='texte_cadeau' style='width:475px'><small>Offre valable jusqu'au 17 juin dans la limite des stocks disponibles. </small></p>";
            var yslOnglet = new ongletSupp("Mon Cadeau", yslTexte, 0);         
            
            popupAll = 0;
            popup = new PopupMarketing("ysl", "javascript:void(0)", "ysl", "popup_parfum popup_parfum_auto", 0, "null");
        }*/
        
        /* bandeau + onglet visible sur tout l'espace Shiseido : https://www.origines-parfums.com/shiseido.htm*/
        if(isMarque("shiseido") == true){ 
            var shiseidoTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/onglet_supp_shiseido.png' style='width: 200px !important; float: left; height: 200px;' /></p><p class='texte_cadeau' style='width:475px'><br />Votre adaptateur de voyage OFFERT, dès 65€ d'achat dans la marque</p><p class='texte_cadeau' style='width:475px'><br />Votre code cadeau :<span style='color:#3c9dc6;'> TRAVEL </span></p><p class='texte_cadeau' style='width:475px'><br /><small>A renseigner dans la case prévue à cet effet, lors de la validation de la commande. </small></p><p class='texte_cadeau' style='width:475px'><small>Offre valable jusqu'au 04 Juin dans la limite des stocks disponibles.</small></p>";
            var shiseidoOnglet = new ongletSupp("Mon Cadeau", shiseidoTexte, 0);         
            
            popupAll = 0;
            popup = new PopupMarketing("shiseido", "http://bit.ly/2KlOA5g", "shiseido", "popup_parfum popup_parfum_auto popup_decouv popup_decouv_transparent", 0, "null");
        }
        
        /* bandeau + onglet visible sur tout l'espace Biotherm : https://www.origines-parfums.com/biotherm.htm*/
        if(isMarque("biotherm") == true){    
            var biothermTexte = "<p class='image_cadeau'><img src='https://www.origines-parfums.com/media/onglet_supp_bio.png' style='width: 200px !important; float: left; height: 200px;' /></p><p class='texte_cadeau' style='width:475px'><br />Votre Trousse de toilette pour homme OFFERTE dès 30 € d'achats dans la marque Biotherm.</p><p class='texte_cadeau' style='width:475px'><br />Votre code cadeau :<span style='color:#38a983;'> POWER </span></p><p class='texte_cadeau' style='width:475px'><br /><small>A renseigner dans la case prévue à cet effet, lors de la validation de la commande. </small></p><p class='texte_cadeau' style='width:475px'><small>Offre valable jusqu'au 4 Juin dans la limite des stocks disponibles. </small></p>";
            var biothermOnglet = new ongletSupp("Mon Cadeau", biothermTexte, 0);         
            
            popupAll = 0;
            popup = new PopupMarketing("biotherm", "javascript:void(0)", "biotherm", "popup_parfum popup_parfum_auto", 0, "null");
        }     
        /* bandeau visible sur soin homme : https://www.origines-parfums.com/soins-homme.htm*/
        if(whereAreYou("list_663768") == true){ 
            popupAll = 0;
            popup = new PopupMarketing("list_663768", "https://www.origines-parfums.com/biotherm.htm", "biotherm", "popup_parfum popup_parfum_auto", 0, "null");
        } 
      
        /* bandeau visible sur cette page : https://www.origines-parfums.com/maquillages-teint.htm*/
        if(whereAreYou("list_663666") == true && detectBrowser() > 639){        
            popupAll = 0;
            popup = new PopupMarketing("list_663666", "https://goo.gl/TejvMD", "toucheeclat_allinglow_bdp", "popup_parfum popup_parfum_auto popup_decouv popup_decouv_transparent popup_gif", 0, "null");
        } 
	  
        /*code qui s'applique sur cette page : https://www.origines-parfums.com/maison-martin-margiela.htm*/
        if(isMarque("margiela") == true && !document.getElementById("mmd-web-embeded")){
            if(whereAreYou("list_664960") == true){
                if(document.getElementById("list_contener").querySelectorAll(".logo_container")[0]){ 
                    var logoC =  document.getElementById("list_contener").querySelectorAll(".logo_container");
                    for(i=0;i<logoC.length;i++){
                        logoC[i].style.display = "none";
                    }
                }                
            }  
            
            /*affichage d'un second onglet mon étiquette si le produit est concené par la personnalisation*/
            if(document.getElementById("product") && document.getElementById("feature") && document.getElementById("feature").querySelector("p.pd01")){
                var margielaTexte1 = "<div class='video_container_product little' id='video_container' style='opacity: 1;'><video controls='' controlsList='nodownload' id='video' poster='../../media/img_deb_fin_margiela.jpg' preload='true' width='300' height='300'><source src='../../media/Selfbridges.mp4' type='video/mp4'></video><img id='img_video' src='../../media/img_deb_fin_margiela.jpg' style='display: none;'><button class='btn_replay2' height='20px' id='button2' onclick='restart()' value='Lecture' style='display: none;'></button></div><div class='writer'><img alt='' src='../media/visuel-machine-a-ecrire.jpg' title='' width='100%' height='100%'></div>";
                var margielaOnglet1 = new ongletSupp("Mon étiquette", margielaTexte1, 2);                
            }
        }   
        /*code qui désactive le bandeau bas de page suivant : "Nous utilisons des cookies pour améliorer notre site et votre expérience d'achat. En utilisant notre site, vous acceptez notre politique de cookies." sur la page diagnostic margiela*/
        if(document.getElementById("mmd-web-embeded")){
           closeCookieBar();
        }                    
		/*Fin de la liste de Laurie*/
        /*Le code suivant rajoutera dans la liste de la mer(/la-mer.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_665513") == true) {
			if (document.getElementById("ff_center")) {
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_ysl' style='color:#2ac495;' href='https://www.origines-parfums.com/jean-paul-gaultier-eaux-fraiches.htm'>GAGNEZ VOTRE CADEAU</a>";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}          
		} 
        
        /*Le code suivant rajoutera dans la liste de la mer(/la-mer.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_729202") == true) {
			if (document.getElementById("ff_center")) {
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_ysl' style='color:#2ac495;' href='https://www.origines-parfums.com/la-mer-decouvrir.htm'>5 façon d'aimer la mer</a>";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}          
		} 
        
         /*Le code suivant rajoutera dans la liste de cacharel (cacharel.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_664495") == true) {
			if (document.getElementById("ff_center")) {
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_ysl' style='color: #2ac495;' href='https://www.origines-parfums.com/cacharel-la-nouveaute-yes-i-am.htm'>La nouveauté Yes I Am</a>";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}          
		}        
        
        /*Le code suivant rajoutera dans la liste de christophe robin (christophe-robin.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_714832") == true) {
			if (document.getElementById("ff_center")) {
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_ysl' style='color:#E00000;' href='https://www.origines-parfums.com/christophe-robin-philosophie.htm'>La philosophie de Christophe Robin</a>";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}          
		} 
        
        /*Le code suivant rajoutera dans la liste de frederic malle (frederic-malle.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_734775") == true) {
			if (document.getElementById("ff_center")) {
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_ysl' style='color:#cf2200' href='https://www.origines-parfums.com/frederic-malle-histoire.htm'>Frederic Malle<br />éditeur de parfumeurs</a>";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}          
		} 
        
       /*Le code suivant rajoutera dans la liste de guerlain (guerlain.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_668947") == true) {
			if (document.getElementById("ff_center")) {
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_guerlain' style='color:#EA1479;' href='https://www.origines-parfums.com/guerlain-rouge-g.htm '>ROUGE G DE GUERLAIN<br /><br /><a class='link_guerlain' href='guerlain-mon-guerlain-nouveau-eau-de-parfum-florale.htm'>Mon Guerlain<br />Eau de parfum florale</a>";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}          
		}         		
         /*Le code suivant rajoutera dans la liste de parfums-rares (/parfums-rares.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_670616") == true) {
			if (document.getElementById("ff_center")) {
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_ysl' style='color: #2ac495;' href='https://www.origines-parfums.com/maison-martin-margiela-diagnostic.htm'>Faites le test<br /> Maison Margiela</a>";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
                document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}          
		} 
        
        /*Le code suivant rajoutera dans la liste de martin margiela (maison-martin-margiela.htm) un lien au dessus des filtres*/
        if (whereAreYou("list_664960") == true) {      
			if (document.getElementById("ff_center")) {               
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_guerlain' style='color: #2ac495;' href='https://www.origines-parfums.com/maison-martin-margiela-diagnostic.htm'>Test : Quel REPLICA est fait pour vous ? </a><br /><a class='link_guerlain' style='' href='https://www.origines-parfums.com/maison-martin-margiela-mon-etiquette.htm'>Personnalisez<br /> votre fragrance</a>";
				document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
				document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}
		}     
		
		/*Le code suivant rajoutera dans la liste d'yves saint laurent (yves-saint-laurent.htm) un lien au dessus des filtres*/
		if (whereAreYou("list_667718") == true) {
			if (document.getElementById("ff_center")) {                
				document.getElementById("ff_center").getElementsByTagName("div")[0].innerHTML = "<a class='link_guerlain' style='color: #961951 !important;' href='yves-saint-laurent-content-page-black-opium.htm'>Découvrez Black Opium</a><a class='link_ysl' href='https://www.origines-parfums.com/yves-saint-laurent-content-page-y.htm'>Y, Le nouveau parfum</a>";
				document.getElementById("ff_center").getElementsByTagName("div")[0].style.display = "block";
				document.getElementById("ff_center").getElementsByTagName("div")[0].style.height = "auto";
			}
		}
		
		if (whereAreYou("prod_9568035") == true) {
			if (document.getElementById("product") && document.getElementById("product").getElementsByTagName("h1")[0] && document.getElementById("product").getElementsByTagName("h1")[0]) {
				document.getElementById("product").getElementsByTagName("h1")[0].getElementsByTagName("a")[0].setAttribute("href", "https://www.origines-parfums.com/vernis-a-levres-ysl.htm");
			}
		}     
        if(document.getElementById("showcase_parent_webzineNew2")){            
            if (document.getElementById("video")) {
                mise_en_page_video_start();
                setTimeout(function () {
                    document.getElementById("video").play();
                }, 3000);          
            }
        }  
        /*le code suivant gére la personnalisation des étiquettes pour les produits margiela concernés*/        
        if(document.getElementById("product") && document.getElementById("feature") && document.getElementById("feature").querySelector("p.pd01")){
            var emE = document.createElement("p");        
            emE.innerHTML = "<br />Ecrivez un mot doux pour en faire un cadeau unique. <a href='../../maison-martin-margiela-mon-etiquette.htm'><u>Découvrir</u></a>";
            document.getElementById("feature").querySelector(".pd01").insertBefore(emE, document.getElementById("ctx_perso"));
        
            i = 0;
            var inputPerso, inputPerso1, inputPerso2
            while(i<2){
                i==0 ? inputPerso = inputPerso1 : inputPerso = inputPerso2;
                inputPerso = document.createElement("input");
                inputPerso.setAttribute("class","ibox");
                inputPerso.setAttribute("name","ctx_perso"+i);
                inputPerso.setAttribute("id","ctx_perso"+i);
                inputPerso.setAttribute("size","10");
                inputPerso.setAttribute("maxlength","22");
                inputPerso.setAttribute("type","text");
                if(i==1){   
                    inputPerso.setAttribute("value","Votre nom ici ou celui d\'un être cher (22 caractères max espaces compris)");
                    inputPerso.setAttribute("onblur","if(this.value == '') { this.value='Votre nom ici ou celui d\\'un être cher (22 caractères max espaces compris)';this.removeAttribute('style');}");  
                }else{
                    inputPerso.setAttribute("value","Votre souvenir ici (22 caractères max espaces compris)");
                    inputPerso.setAttribute("onblur","if(this.value == '') { this.value='Votre souvenir ici (22 caractères max espaces compris)';this.removeAttribute('style');}");
                }

                inputPerso.setAttribute("onfocus","this.value=''");              
                inputPerso.setAttribute("onclick","this.style.color='black';");  
                document.getElementById("feature").querySelector(".pd01").insertBefore(inputPerso, document.getElementById("ctx_perso"));  
                i++;
            }
        
            var btnE = document.createElement("input");
            btnE.setAttribute("class","btn_buy");
            btnE.setAttribute("value","Valider");
            btnE.setAttribute("tabindex","4");
            btnE.setAttribute("type","button");
            btnE.setAttribute("onclick","margielaPersonnalize()");
            document.getElementById("feature").querySelector(".pd01").insertBefore(btnE, document.getElementById("ctx_perso0"));  
        
            var emE2 = document.createElement("p");   
            emE2.setAttribute("class","citation");
            emE2.innerHTML = "<br />* Pour une personnalisation compléte, votre parfum sera ouvert afin de coller l'étiquette.<br /> Puis il sera réemballé avec soin. En validant, vous acceptez ces conditions. Aucun retour possible. ";
            document.getElementById("feature").querySelector(".pd01").insertBefore(emE2, document.getElementById("ctx_perso"));

            if(document.getElementById("contener_tab")){
                document.getElementById("contener_tab").addEventListener("click", function(){                
                    if(document.getElementById("briefcase_product")){                    
                        if(document.getElementById("briefcase_product").className=="briefcase_texte_on"){                                           document.getElementById("feature").querySelector(".pd01").style.top=document.getElementById("contener_content").offsetHeight+555+"px";
                        }else{
                            document.getElementById("feature").querySelector(".pd01").removeAttribute("style");
                        }
                    }
                });
            }

            document.getElementById("product").className += " product_pd01";  
           document.getElementById("contenances_contener").querySelector("input#btn_basket_buy").addEventListener("click",function(){
                document.getElementById("ctx_perso").value="";
                document.getElementById("txt_alertStock").innerHTML="Rupture de stock"; 
            });           
            
            closeCookieBar();
        } 
        if(document.getElementById("content_popup_bsk_onglet")){
            document.getElementById("content_popup_bsk_onglet").innerHTML="<b>Chers clients</b>, à compter de ce jour nous ne garantissons plus la livraison pour le 24 décembre 2017.";
        }
        if(document.getElementById("basket_notempty")){   
            var basketInfo = document.getElementById("basket_notempty").querySelector(".basket_info");
            basketInfo.innerHTML = basketInfo.innerHTML.replace("Chers clients","<b>Chers clients</b>");
            if(detectBrowser()<640){
                document.getElementById("basket").insertBefore(basketInfo, document.getElementById("basket_notempty"));
            }else{
               document.getElementById("basket_notempty").querySelector(".btn_pos").appendChild(basketInfo); 
            }            
        }
        /*le contenu du if à remplacer whereAreYou("prod_IDStore") == true quand le produit sera créé + dans la fonction l'url de l'ecrin quand il sera créé*/
        if (whereAreYou("prod_10472783") == true){ 
            productDuo("../rouge-g-de-guerlain-l-ecrin-double-miroir.htm","banniere-choisissez-votre-ecrin-701x80.png","rouge_g_ecrin_buy","rouge_g_recharge_buy");           
            document.getElementById("product").className += " vue_supp_ban";   
        }
        /*le contenu du if à remplacer whereAreYou("prod_IDStore") == true quand le produit sera créé + dans la fonction l'url de l'ecrin quand il sera créé*/
        if (whereAreYou("prod_10472917") == true){
            productDuo("../le-rouge-g-de-guerlain.htm","banniere-choisissez-votre-teinte-701x80.png","rouge_g_recharge_buy","rouge_g_ecrin_buy");       
            document.getElementById("product").className += " vue_supp_ban";    
        }
        if(document.location=="https://www.origines-parfums.com/mag/fr/shoppingcart.php"){
            if(readCookie("rouge_g_recharge_buy")){ eraseCookie("rouge_g_recharge_buy"); }
            if(readCookie("rouge_g_ecrin_buy")){ eraseCookie("rouge_g_ecrin_buy"); }
        }
        if(document.getElementById("ctx_advantage")){
            document.getElementById("btn_advantage").addEventListener("click",function(){
                if(document.getElementById("ctx_advantage").value=="V&RCADEAU"){
                    document.getElementById("ctx_advantage").value="VRCADEAU";
                }                
            });
        }
        
        /*code qui affiche un popup video dans cette https://www.origines-parfums.com/la-mer-decouvrir.htm si on clique sur la 1er banniere*/
        if(whereAreYou("list_738729")==true){
            var videoMer = "<div class='video_container_product' id='video_container'><video controls='' poster='../../media/la_mer_decouvrir_video.jpg' id='video' preload='true' width='100%' height='auto'><source src='https://oui-lemag.com/Videos/la_mer.mp4' type='video/mp4' /></video><img id='img_video' src='../../media/la_mer_decouvrir_video.jpg' /><button class='btn_replay2' type='button' height='20px' id='button2' onclick='restart()' value='Lecture'></button></div>";
            var goVideo = document.getElementById("container").getElementsByTagName("div")[0];
            
            popupVideo(videoMer,goVideo);
        }    
	}
	
	if (document.getElementById("calendrierAvent")) {
		avent();
        if(document.location=="https://www.origines-parfums.com/calendrier-avent-parfum.htm#rules"){
            var text = "<p class='texte_avantage'>Pour patienter jusqu'à No&euml;l, chaque jour les plus grandes marques vous offrent une nouvelle surprise à découvrir.</p><p class='texte_avantage'><span onclick=showDiv('alertMiniQuantityPurchase')>&gt; CLIQUEZ VITE SUR LA FENÊTRE DU JOUR ! &gt;</span></p>";
            PopupOuvrant("jeu_selection", text);	
            if(document.getElementById("sf_pop_ouvrant")) { 
                document.getElementById("sf_pop_ouvrant").className += " sf_pop_ouvrant sf_pop_avent"; 
            }
        }        
	}
    
	if (whereAreYou("list_726149") == true || whereAreYou("list_734783") == true) {        
		item = document.getElementById("list_contener").querySelectorAll("div.pdt");
		if (item) {            
			var viewProdElm = document.getElementById("list_contener").querySelectorAll("img.view");
			var viewProd = new Image(); 
			for(i = 0; i < viewProdElm.length; i++){
				if(viewProdElm[i].getAttribute("src").indexOf("s.jpg") > - 1){
				   viewProd.src = viewProdElm[i].getAttribute("src").replace("s.jpg","hd.jpg");	
				}
				if(viewProdElm[i].getAttribute("src").indexOf("s.png") > - 1){
					viewProd.src = viewProdElm[i].getAttribute("src").replace("s.png","hd.png");	
				}
					viewProdElm[i].src = viewProd.src;
					item[i].parentNode.className="carousel-cell";
			}			
								
			document.getElementById("list_contener").setAttribute("class", "flickity_sf");
			document.getElementById("list_contener").parentNode.style.display = "block";
								
			document.getElementById("list_contener").removeChild(document.getElementById("list_contener").getElementsByTagName("div")[0]);
            var inter = document.getElementById("list_contener").querySelectorAll("img.inter");
			for(var i = 0; i < inter.length; i++){
			   document.getElementById("list_contener").removeChild(inter[i]);
			}            
			var inter_bottom = document.getElementById("list_contener").querySelectorAll("img.inter_bottom");
			for(var i = 0; i < inter_bottom.length; i++){
			   document.getElementById("list_contener").removeChild(inter_bottom[i]);
			}			
							
			for(i = 0; i < item.length; i++){
				item[i].insertBefore(item[i].getElementsByTagName("span")[5],item[i].getElementsByTagName("span")[1]);
			}
					
			var carousel_elements = [], carousel_clones = [];
			var j = 0;	
			var element;
			for (i = 0; i < item.length; i++) {		
				element = item[i];
				clone = element.cloneNode(true);
				carousel_elements[j] = item[i];
				carousel_clones[j] = clone;
				j++;			
			}
			if (j <= 3) {
				for (i = 0; i < item.length; i++) {					
					element = item[i];
					clone = element.cloneNode(true);
					carousel_clones[j] = clone;
					j++;					
				}
			}
			carousel_elements = carousel_elements.concat(carousel_clones);	
			nb_elements = carousel_elements.length;
			for (i = 0; i < nb_elements; i++) {
				document.getElementById("flickity_content0").appendChild(carousel_elements[i]);
			}		
					
			if(document.getElementById("title_associed")){
			   document.getElementById("title_associed").className="grey-separator";
				document.getElementById("title_associed").innerHTML = "<span>Nouveautés</span>";			
			}
		}
		setTimeout(function(){
			var elem = document.getElementById("flickity_content0"); 
            var hrefElem;
            if(elem){
                closeCookieBar();
                var aElem = elem.getElementsByTagName("a");
                for(i = 0; i < aElem.length; i++){
                    hrefElem = aElem[i].getAttribute("href");
                    aElem[i].setAttribute("href",hrefElem+"#pr");
                }
                var flkty = new Flickity( elem, {
                    autoPlay: 5000,
                    contain: true,
                    setGallerySize: false,
                    resize: false,				
                    freeScroll: true,
                    wrapAround: true,
                    pageDots: false,
                    percentPosition: true,
                    initialIndex: 3
                });
                var elem1 = document.getElementById("container-tom-ford"); 
                var flkty = new Flickity( elem1, {
                    autoPlay: 5000,
                    contain: true,
                    setGallerySize: false,
                    resize: false,				
                    freeScroll: true,
                    wrapAround: true,
                    pageDots: false,
                    percentPosition: true,
                    initialIndex: 3
                });
                fade('in', 500, document.getElementById('brands-tab'));
                fade('out', 600, document.getElementById('choose'));
                fade('in', 600, document.getElementById('close'));
            }           
		}, 200);
					
		if(document.getElementById("brands-parfums")){
			document.getElementById("brands-parfums").removeAttribute("style");
		}		
	}
    if (whereAreYou("page_729247") == true) {
        
        if (val.indexOf("iemobile") == -1 && val.indexOf("iphone") > -1) {
            if (window.innerWidth == 375) {
                if(val.indexOf("applewebkit/600.1.4") > -1){
                    document.getElementById("master").parentNode.className += " iphone6";
                }
                 if(val.indexOf("applewebkit/601.1.46") > -1){
                    document.getElementById("master").parentNode.className += " iphone6s";
                }
            }
            if (window.innerWidth == 414) {
                if(val.indexOf("applewebkit/600.1.4") > -1){
                    document.getElementById("master").parentNode.className += " iphone6plus";
                }else{
                    document.getElementById("master").parentNode.className += " iphone6splus";
                }
            }
            if (window.innerWidth == 667) {
                if(val.indexOf("applewebkit/600.1.4") > -1){
                    document.getElementById("master").parentNode.className += " iphone6";
                }
                 if(val.indexOf("applewebkit/601.1.46") > -1){
                    document.getElementById("master").parentNode.className += " iphone6s";
                }
            }
        }
        if (val.indexOf("iemobile") == -1 && val.indexOf("android") > -1) {
            document.getElementById("master").parentNode.className += " android";
        }       
        setTimeout(function(){
            var elem = document.querySelectorAll(".carousel-emballages")[0];
            var flkty = new Flickity( elem, {
                imagesLoaded: true,
                initialIndex: 0, 
                freeScroll: true, 
                wrapAround: true, 
                pageDots: false 				
            });	
            var elem1 = document.querySelectorAll(".carousel-emballages")[1];
            var flkty = new Flickity( elem1, {
                imagesLoaded: true,
                initialIndex: 0, 
                freeScroll: true, 
                wrapAround: true, 
                pageDots: false 				
            });	
        }, 400);  
    }
    var help = HgetElementsByClassName("help", "li")[0];
	if (help) {
		help.innerHTML = "<a href=/mag/fr.php><span>FR</span></a> <span>/</span> <a href=/mag/en.php><span>EN</span></a> <span>/</span> <a href=/mag/de.php><span>DE</span></a>";
		if (document.getElementsByTagName("meta")[1]) {
            if (document.getElementsByTagName("meta")[1].getAttribute("content") == "en") {
				help.getElementsByTagName("span")[2].className = "black";
				help.getElementsByTagName("span")[0].removeAttribute("class");
                help.getElementsByTagName("span")[4].removeAttribute("class");
				document.getElementById("flag").getElementsByTagName("span")[1].className = "black";
				document.getElementById("flag").getElementsByTagName("span")[0].removeAttribute("class");
                document.getElementById("flag").getElementsByTagName("span")[2].removeAttribute("class");
			}
			if (document.getElementsByTagName("meta")[1].getAttribute("content") == "de") {
				help.getElementsByTagName("span")[4].className = "black";
				help.getElementsByTagName("span")[0].removeAttribute("class");
                help.getElementsByTagName("span")[2].removeAttribute("class");
				document.getElementById("flag").getElementsByTagName("span")[2].className = "black";
				document.getElementById("flag").getElementsByTagName("span")[0].removeAttribute("class");
                document.getElementById("flag").getElementsByTagName("span")[1].removeAttribute("class");
			}
			if (document.getElementsByTagName("meta")[1].getAttribute("content") == "fr") {
				help.getElementsByTagName("span")[0].className = "black";
				help.getElementsByTagName("span")[2].removeAttribute("class");
                help.getElementsByTagName("span")[4].removeAttribute("class");
				document.getElementById("flag").getElementsByTagName("span")[0].className = "black";
                document.getElementById("flag").getElementsByTagName("span")[1].removeAttribute("class");
				document.getElementById("flag").getElementsByTagName("span")[2].removeAttribute("class");
			}
		}
	}
    if (document.getElementsByTagName("meta")[1] && document.getElementsByTagName("meta")[1].getAttribute("content") == "de") {	
       if(document.getElementById("home")){
           if(document.getElementById("tc_03") && document.getElementById("tc_03").querySelector(".priceindicative")){
               document.getElementById("tc_03").querySelector(".priceindicative").innerHTML = document.getElementById("tc_03").querySelector(".priceindicative").innerHTML.replace("Au lieu de","Anstatt");
           }
       } 
    }
    if(document.getElementById("ff_crit_att3464")){     
        var brands = document.getElementById("ff_crit_att3464");
        var brandsP = document.getElementById("ff_center").getElementsByTagName("div")[2];
        document.getElementById("ff_center").insertBefore(brands,brandsP);
    }
}